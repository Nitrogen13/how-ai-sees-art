import os

import skimage

from consts import COCO_MODEL_PATH, MODEL_DIR
from mask_rcnn.coco.coco import CocoConfig

# Import Mask RCNN
from mrcnn import utils
import mrcnn.model as modellib

# Download COCO trained weights from Releases if needed
from mask_rcnn.utils import class_names, save_result

if not os.path.exists(COCO_MODEL_PATH):
    utils.download_trained_weights(COCO_MODEL_PATH)


class InferenceConfig(CocoConfig):
    # Set batch size to 1 since we'll be running inference on
    # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1


# Create model object in inference mode.
model = modellib.MaskRCNN(
    mode="inference", model_dir=MODEL_DIR, config=InferenceConfig()
)

# Load weights trained on MS-COCO
model.load_weights(COCO_MODEL_PATH, by_name=True)


def inference(image_path, processed_image_path):
    image = skimage.io.imread(image_path)
    result = model.detect([image], verbose=0)[0]
    save_result(
        image,
        result["rois"],
        result["masks"],
        result["class_ids"],
        class_names,
        result["scores"],
        processed_image_path,
    )
