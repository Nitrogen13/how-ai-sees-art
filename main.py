import os
import shutil
import random

from fastapi import FastAPI, File, UploadFile
from starlette.requests import Request
from starlette.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates

from consts import IMAGES_PATH, PROCESSED_IMAGES_PATH, OBJECT_IS_NOT_DETECTED, NOT_IMAGE
from mask_rcnn.mask_rcnn_model import inference

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")
app.mount(
    "/processed_images",
    StaticFiles(directory="processed_images"),
    name="processed_images",
)
templates = Jinja2Templates(directory="templates")

processed_images = [
    os.path.join(PROCESSED_IMAGES_PATH, x)
    for x in os.listdir(PROCESSED_IMAGES_PATH)
    if os.path.isfile(os.path.join(PROCESSED_IMAGES_PATH, x))
]


@app.get("/")
async def get_image(request: Request):
    image = random.choice(processed_images)
    return templates.TemplateResponse("base.html", {"request": request, "image": image})


@app.post("/upload")
async def upload_image(request: Request, file: UploadFile = File(...)):
    if "image" in file.content_type:
        with open(IMAGES_PATH / file.filename, "wb+") as upload_folder:
            shutil.copyfileobj(file.file, upload_folder)
        image_path = os.path.join(IMAGES_PATH, file.filename)
        processed_image_path = os.path.join(PROCESSED_IMAGES_PATH, file.filename)
        inference(image_path, processed_image_path)
        if os.path.exists(processed_image_path):
            processed_images.append(processed_image_path)
            return templates.TemplateResponse(
                "base.html", {"request": request, "image": processed_image_path}
            )
        else:
            return templates.TemplateResponse(
                "error.html", {"request": request, "text": OBJECT_IS_NOT_DETECTED}
            )
    else:
        return templates.TemplateResponse(
            "error.html", {"request": request, "text": NOT_IMAGE}
        )
