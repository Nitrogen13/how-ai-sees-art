import os

from tqdm import tqdm

from consts import IMAGES_PATH, PROCESSED_IMAGES_PATH
from mask_rcnn.mask_rcnn_model import inference

images = []
for root, subdirs, files in os.walk(IMAGES_PATH):
    for filename in files:
        images.append((filename, os.path.join(root, filename)))


for title, image_path in tqdm(images):
    try:
        inference(image_path, PROCESSED_IMAGES_PATH / title)
    except:
        print(f"Failed {title}")
