from pathlib import Path

ROOT_PATH = Path(__file__).parent
IMAGES_PATH = ROOT_PATH / "images"
PROCESSED_IMAGES_PATH = ROOT_PATH / "processed_images"

# Local path to trained weights file
COCO_MODEL_PATH = str(ROOT_PATH / "mask_rcnn" / "mask_rcnn_coco.h5")

# Directory to save logs and trained model
MODEL_DIR = str(ROOT_PATH / "logs")

OBJECT_IS_NOT_DETECTED = "Sorry I could not find any object in your image"
NOT_IMAGE = "Please upload only images"
